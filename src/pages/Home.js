import { useEffect, useState } from "react";
import Featured from '../components/Featured';
import Drinks from '../components/Drinks'
import Foods from '../components/Foods'
import { Helmet, HelmetProvider } from 'react-helmet-async';

export default function Home(){
    const [title, setTitle] = useState('')

    useEffect(() => {
        setTitle("Café Leblanc")
    }, [title])

    return(
        <HelmetProvider>
        <Helmet>
        <title>{title ? title: "Café Leblanc"}</title>
        </Helmet>
        <Featured />
        <Drinks />
        <Foods />
        </HelmetProvider>
        )
    }