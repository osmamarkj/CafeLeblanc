import React, { useContext, useEffect, useState } from 'react';
import {Container, Button} from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function Error(){

    const { user } = useContext(UserContext)
    const [title, setTitle] = useState('')

    useEffect(() => {
        setTitle('Page not found | Café Leblanc')
    }, [])

    return(
        <HelmetProvider>
        <Helmet>{ <title>{ title ? title : "Café Leblanc" }</title> }</Helmet>
        <Container className="mt-5 text-center mb-4">
        <Container>

        <Image src="/images/e404.png" />
        </Container>
        <h1>404 - Page not Found</h1>
        <p>The page you're looking for does not exist</p>
        {
            user.isAdmin ?
            <Button className="reg-btn" as={Link} to="/allProducts">Go Back to Products</Button>
            :
            <Button className="reg-btn" as={Link} to="/">Continue Shopping</Button>
        }
        </Container>
        </HelmetProvider>
        )
}