import React, { useState, useEffect, useContext } from 'react';
import { 
    Container,
    Table, 
    Button,
    Col,
    Image
} from 'react-bootstrap'
import { Link, Navigate } from 'react-router-dom';
import {FiMinus, FiPlus} from 'react-icons/fi'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function MyCart(){

    const {user} = useContext(UserContext)
    const [total, setTotal] = useState(0)
    const [cart, setCart] = useState([])
    const [tableRows, setTableRows] = useState([])
    const [redirect, setRedirect] = useState(false)
    const [title, setTitle] = useState('')
    const [cartStatus, setCartStatus] = useState('')

    useEffect(() => {
        if(localStorage.getItem('cart')){
            setCart(JSON.parse(localStorage.getItem('cart')))
        }
    }, [])

    
    useEffect(() => {
        let cartItems = cart.map((item) => {
            const src=`/images/products/${item.productId}.png`

            return(
                <>

                <tr key={item.productId} className="d-none d-md-table-row">
                <td>
                <Link to ={`/product/${item.productId}`}><Image src={src} className="all-image"/></Link>
                </td>
                <td className="align-middle text-center">
                <Link to ={`/product/${item.productId}`} className="product-name ms-2">{item.name}</Link>
                </td>
                <td 
                className="align-middle text-center cart-price w-25 d-sm-none d-lg-table-cell">
                ₱{item.price.toFixed(2)}
                </td>
                <td className="align-middle text-center">
                <div className="cart-qty">

                {item.quantity == 1 ?
                    <Button 
                    className="quantity-btn minus" variant="outline-dark" disabled 
                    onClick={() => qtyBtn(item.productId, "sub")}
                    >
                    <FiMinus></FiMinus>
                    </Button>
                    :
                    <Button 
                    className="quantity-btn minus" variant="outline-dark" 
                    onClick={() => qtyBtn(item.productId, "sub")}
                    >
                    <FiMinus></FiMinus>
                    </Button>
                }
                {item.quantity}
                <Button 
                className="quantity-btn add" variant="outline-dark"
                onClick={() => qtyBtn(item.productId, "add")}
                >
                <FiPlus></FiPlus>
                </Button>

                </div>
                <Button 
                className="mt-3 remove-btn"
                onClick={() => removeBtn(item.productId)} 
                variant="none">
                Remove
                </Button>
                </td>
                <td className="text-center align-middle">₱{item.subtotal.toFixed(2)}</td>
                </tr>

            {/* SMALLER SCREEN */}

            <tr key={`sm${item.productId}`} className="d-md-none">
            <td>
            <Link to ={`/product/${item.productId}`}><Image src={src} className="all-image"/></Link>
            </td>
            <td className="align-middle text-center">
            <Link to ={`/product/${item.productId}`} className="product-name ms-2">{item.name}</Link>
            <h6>₱{item.price}</h6>
            </td>
            <td className="align-middle text-center">
            <div className="cart-qty">

            {item.quantity == 1 ?
                <Button 
                className="quantity-btn minus" variant="outline-dark" disabled 
                onClick={() => qtyBtn(item.productId, "sub")}
                >
                <FiMinus></FiMinus>
                </Button>
                :
                <Button 
                className="quantity-btn minus" variant="outline-dark" 
                onClick={() => qtyBtn(item.productId, "sub")}
                >
                <FiMinus></FiMinus>
                </Button>
            }
            {item.quantity}
            <Button 
            className="quantity-btn add" variant="outline-dark"
            onClick={() => qtyBtn(item.productId, "add")}
            >
            <FiPlus></FiPlus>
            </Button>

            </div>
            <Button 
            className="mt-3 remove-btn"
            onClick={() => removeBtn(item.productId)} 
            variant="none">
            Remove
            </Button>
            </td>
            </tr>
            </>
            )

        })
        setTableRows(cartItems)
        let tempTotal = 0

        cart.forEach((item) => {
            tempTotal += item.subtotal
        })

        setTotal(tempTotal)

        if(cart.length < 1) {
            setCartStatus('Your cart is empty.')
            setTitle('Your cart is empty | Café Leblanc')
        } else {
            setCartStatus(`Your Shopping Cart (${cart.length})`)
            setTitle('Cart | Café Leblanc')
        }
    }, [cart, title])


    // Quantity Buttons
    const qtyBtn = (productId, operator) => {
        let tempCart = [...cart]

        for (let i = 0; i < tempCart.length; i++){
            if(tempCart[i].productId === productId){
                if(operator == "add"){
                    tempCart[i].quantity += 1
                    tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
                } else if(operator == "sub"){
                    tempCart[i].quantity -= 1
                    tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity 
                }
            }
        }
        
        localStorage.setItem('cart', JSON.stringify(tempCart))
        setCart(tempCart)
    }

    // Remove Button
    const removeBtn = (productId) => {
        let tempCart = [...cart]

        let cartId = tempCart.map((item) => {
            return item.productId
        })

        tempCart.splice([cartId.indexOf(productId)], 1)

        setCart(tempCart)
        localStorage.setItem('cart', JSON.stringify(tempCart))
    }

    const checkOut = () => {
        const checkOutItem = cart.map((item) => {
            return {
                productId: item.productId,
                productName: item.name,
                quantity: item.quantity
            }
        })

        fetch(`https://hidden-stream-68962.herokuapp.com/users/checkOut`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                products: checkOutItem
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Swal.fire({
                    title: 'Order Success!',
                    text: 'We received your order!',
                    icon: 'success'
                })
                localStorage.removeItem('cart')

                setRedirect(true)
            } else {
                alert("Something went wrong.")
            }
        })
    }

    return (
        <HelmetProvider>
        <Helmet>
        <title>{ title ? title : "Café Leblanc" }</title>
        </Helmet>
        {
            (user.isAdmin) ?
            <Navigate to="/" />
            :
            (redirect) ?
            <Navigate to ="/myOrders" />
            :
            <Container className="cart-container">
            <Container className="">
            {
                cart.length < 1 ?
                <Button variant="outline-dark" className="d-none" as = {Link} to ="/menu">Continue Shopping</Button>
                :
                <Button variant="outline-dark" className="back mt-3" as = {Link} to ="/menu">Continue Shopping</Button>
            }
            <h3 className="mt-4 text-center">{cartStatus}</h3>
            </Container>
            {
                (cart.length == 0) ?
                <Container className="cart-empty-bottom">
                <Image src="/images/empty.png" fluid className="empty" />
                <Button variant="outline-dark" className="back mb-4" as = {Link} to ="/menu">Continue Shopping</Button>
                </Container>
                :
                <>
                <Col md={{offset: 2, span: 8}}>
            {/* For Tablet and Larger Screen */}
            <Table className="mt-3 d-none d-md-table" responsive>
            <thead className="cart-header">
            <tr>
            <th className="text-center">Product</th>
            <th></th>
            <th className="text-center d-md-none d-lg-table-cell">Price</th>
            <th className="text-center">Quantity</th>
            <th className="text-center">Subtotal</th>
            <th></th>
            </tr>
            </thead>
            <tbody>
            {tableRows}
            </tbody>
            </Table>
            <div className="cart-checkout-container d-none d-md-flex">
            <div>
            <h3 className="total-text">Total: ₱{total}</h3>
            </div>
            <div className="cart-checkout-btn mt-2 mb-5">
            {
                user.id == null ?
                <Button as= {Link} to="/login" variant="outline-dark">CheckOut</Button>
                :
                <Button onClick={() => checkOut()} variant="outline-dark">CheckOut</Button>
            }
            </div>
            </div>
        {/* For Tablet and Larger Screen END */}
        <Table className="d-md-none mt-3">
        <tbody>
        {tableRows}
        </tbody>
        </Table>
        <div className="cart-checkout-container-sm d-md-none">
        <div>
        <h3 className="total-text">Total: ₱{total}</h3>
        </div>
        <div className="cart-checkout-btn mt-2 mb-5">
        {
            user.id == null ?
            <Button as= {Link} to="/login" variant="outline-dark">CheckOut</Button>
            :
            <Button onClick={() => checkOut()} variant="outline-dark">CheckOut</Button>
        }
        </div>
        </div>
        </Col>
        </>
    }
    </Container>
}
</HelmetProvider>
)
}