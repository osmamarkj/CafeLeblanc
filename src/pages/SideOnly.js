import { useEffect, useState } from "react";
import { CardGroup, Container } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function Side() {
  const [products, setProducts] = useState([]);
  const [title, setTitle] = useState('')

  useEffect(() => {
    fetch(`https://hidden-stream-68962.herokuapp.com/products/search`, {
      method: "POST",
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        search: "side"
      })
    })
    .then((res) => res.json())
    .then((data) => {
      setProducts(
        data.map((product) => {
          return <ProductCard key={product._id} prodProp={product} />;
        })
        );
    });

    setTitle('Side Dish Menu | Café Leblanc')
  }, [title]);

  return (
    <HelmetProvider>
    <Helmet>
    <title>{ title ? title : "Café Leblanc" }</title>
    </Helmet>
    <Container className="mt-5 text-center mb-5">
    <h2 className="mb-4">Side Dish Collection</h2>
    <CardGroup>{products}</CardGroup>
    </Container>
    </HelmetProvider>
    );
}
