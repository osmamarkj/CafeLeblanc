import React, { useState, useEffect, useContext } from 'react';
import {
    Container,
    Accordion,
    Card,
    Image
} from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function Orders(){

    const {user} = useContext(UserContext)
    const [orderList, setOrderList] = useState([])
    const [title, setTitle] = useState('')

    useEffect(() => {
        fetch(`https://hidden-stream-68962.herokuapp.com/users/myOrders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data.length > 0){
                let orders = data.map((item, index) => {
                    let date = new Date(item.purchasedOn)
                    date = date.toDateString()
                    return(
                        <Card key={item._id}>
                        <Accordion.Item eventKey={index}>

                        <Accordion.Header as={Card.Header}>
                        Order #{index + 1} - Purchased on {date}
                        </Accordion.Header>
                        <Accordion.Body>
                        <Card.Body>
                        <h6>ORDER ID: {item._id}</h6>
                        <h6>Items: </h6> 
                        <ul>
                        {
                          item.products.map((subitem) => {
                              return (
                              <li key={subitem.productId} className="order-list mt-2">
                              <Link to={`/product/${subitem.productId}`} className="order-item">
                              {subitem.name}
                              </Link> - Quantity: {subitem.quantity}
                              </li>
                              )
                          })
                      }
                      </ul> 
                      <h6 className="mt-4">Total: <span>{item.totalAmount}</span></h6>
                      </Card.Body>
                      </Accordion.Body>
                      </Accordion.Item>
                      </Card>
                      )
                })
                setOrderList(orders)
            }
        })
    }, [])

    useEffect(() => {
        if(orderList.length < 1){
            setTitle('No orders | Café Leblanc')
        } else {
            setTitle("Orders | Café Leblanc")
        }
    }, [title, orderList])

    return(
        <HelmetProvider>
        <Helmet>
        <title>{ title ? title : 'Café Leblanc' }</title>
        </Helmet>
        {
            (user.id === null) ? <Navigate to="/login" />
            :
            user.isAdmin ?
            <Navigate to="/allProducts" />
            :
            <Container className="mb-5 order-container">
            <h2 className="text-center mt-3 mb-4">My Orders</h2>
            {
                orderList.length == 0 ?
                <Container className="text-center order-wrapper">
                
                <Image className="order-empty mt-3" src="/images/empty-order.png" />
                <Link to="/menu" className="order-link-text"><h4>You have no order. <span>Go shopping</span></h4></Link>
                </Container>
                :
                <Accordion>
                {orderList}
                </Accordion>
            }
            </Container>
        }
        </HelmetProvider>
        )
    }