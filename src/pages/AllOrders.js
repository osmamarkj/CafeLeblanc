import React, { useState, useEffect, useContext } from 'react';
import {
    Container,
    Accordion,
    Card,
    Col
} from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function AllOrders(){
    const {user} = useContext(UserContext)
    const [orderList, setOrderList] = useState([])
    const [title, setTitle] = useState('')

    useEffect(() => {
        fetch(`https://hidden-stream-68962.herokuapp.com/users/orders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data.length > 0){
                let orders = data.map((item, index) => {
                    let date = new Date(item.purchasedOn)
                    date = date.toDateString()
                    return(
                        <Card key={item._id}>
                        <Accordion.Item eventKey={index}>

                        <Accordion.Header as={Card.Header}>
                        Order ID: {item._id} - Purchased on {date}
                        </Accordion.Header>
                        <Accordion.Body>
                        <h5>Customer: {item.email}</h5>
                        <Card.Body className="text-center">
                        <h6>Items: </h6> 
                        <ul>
                        {
                          item.products.map((subitem) => {
                              return (
                              <li key={subitem.productId} className="order-list mt-2">
                              <Link to={`/product/${subitem.productId}`} className="order-item">
                              {subitem.name}
                              </Link> x {subitem.quantity}
                              </li>
                              )
                          })
                      }
                      </ul> 
                      </Card.Body>
                      <h5 className="mt-4">Total: <span>{item.totalAmount}</span></h5>
                      </Accordion.Body>
                      </Accordion.Item>
                      </Card>
                      )
                })
                setOrderList(orders)
                setTitle('All Orders | Café Leblanc')
            }
        })
    }, [])


    return(
        <HelmetProvider>
        <Helmet>
        <title>{ title ? title : "Café Leblanc" }</title>
        </Helmet>
        {
            !user.isAdmin ?
            <Navigate to="/menu" />
            :
            <Container className="mb-5">
            <h2 className="text-center mt-3 mb-4">Orders</h2>
            <Col lg={{offset: 2, span: 8}}>
            <Accordion>
            {orderList}
            </Accordion>
            </Col>
            </Container>
        }
        </HelmetProvider>
        )
    }