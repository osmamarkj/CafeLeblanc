import { useState, useEffect, useContext } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import UserContext from '../UserContext';
import Swal from "sweetalert2";
import {Link, Navigate} from 'react-router-dom'
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function Register() {

  const {user} = useContext(UserContext)

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState("");
  const [isRegister, setIsRegister] = useState(false);
  const [title, setTitle] = useState('')

  function registerUser(e) {
    e.preventDefault();

    fetch(`https://hidden-stream-68962.herokuapp.com/users/checkEmail`, {
      method: "POST",
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        email: email
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(data){
        Swal.fire({
          title: 'Email Exist',
          text: 'Please provide another email address.',
          icon: 'error'
        })
      } else {
        saveUser();
      }
    })

    const saveUser = () => {
      fetch(`https://hidden-stream-68962.herokuapp.com/users/signup`, {
        method: "POST",
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          mobileNo: mobileNo,
          password: password1
        })
      })
      .then(res => res.json())
      .then(data => {
        if(data){
          Swal.fire({
            title: 'Register Successful',
            text: 'You can now order our delicious products',
            icon: 'success'
          })
          setIsRegister(true)
        } else {
          Swal.fire({
            title: 'Registration Failed',
            text: 'Please try again.',
            icon: 'error'
          })
        }
      })

      setFirstName("");
      setLastName("");
      setEmail("");
      setMobileNo("");
      setPassword1("");
      setPassword2("");
    }
  }

  const notComplete = (e) => {
    const Toast = Swal.mixin({
      toast: true,
      iconColor: 'white',
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      showCloseButton: true,
      customClass: {
        popup: 'colored-toast'
      }
    })
    
    Toast.fire({
      icon: 'error',
      title: 'Please check you information'
    })
  }

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNo !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2 &&
      mobileNo.length === 11
      ) {
      setIsActive(true);
  } else {
    setIsActive(false);
  }

  setTitle("Register | Café Leblanc")
}, [firstName, lastName, email, mobileNo, password1, password2, title]);

  return (
    <HelmetProvider>
    <Helmet>
    <title>{title ? title : "Café Leblanc"}</title>
    </Helmet>
    {
      (isRegister) ?
      <Navigate to="/login" />
      :
      (user.id !== null) ?
      <Navigate to="/" />
      :
      <Container className="mt-5">
      <Row>
      <Col lg={{ span: 5, offset: 3 }} className="form-div">
      <Form className="mb-3" onSubmit={(e) => registerUser(e)}>
      <h1 className="register mb-4">Register</h1>

      <Row>
      <Col>
      <Form.Group className="mb-3" controlId="firstName">
      <Form.Label>First Name</Form.Label>
      <Form.Control
      value={firstName}
      onChange={(e) => setFirstName(e.target.value)}
      type="text"
      placeholder="Enter your first name"
      required
      />
      </Form.Group>
      </Col>
      <Col>
      <Form.Group className="mb-3" controlId="lastName">
      <Form.Label>Last Name</Form.Label>
      <Form.Control
      value={lastName}
      onChange={(e) => setLastName(e.target.value)}
      type="text"
      placeholder="Enter last name"
      required
      />
      </Form.Group>
      </Col>
      </Row>

      <Row>
      <Col>
      <Form.Group className="mb-3" controlId="userEmail">
      <Form.Label>Email address</Form.Label>
      <Form.Control
      value={email}
      onChange={(e) => setEmail(e.target.value)}
      type="email"
      placeholder="Enter email"
      required
      />
      </Form.Group>
      </Col>
      <Col>
      <Form.Group className="mb-3" controlId="mobileNo">
      <Form.Label>Mobile Number</Form.Label>
      <Form.Control
      value={mobileNo}
      onChange={(e) => setMobileNo(e.target.value)}
      type="text"
      placeholder="Enter your mobile"
      required
      />
      </Form.Group>
      </Col>
      </Row>

      <Row>
      <Col>
      <Form.Group className="mb-3" controlId="password1">
      <Form.Label>Password</Form.Label>
      <Form.Control
      value={password1}
      onChange={(e) => setPassword1(e.target.value)}
      type="password"
      placeholder="Password"
      required
      />
      </Form.Group>
      </Col>
      <Col>
      <Form.Group className="mb-3" controlId="password2">
      <Form.Label>Verify your password</Form.Label>
      <Form.Control
      value={password2}
      onChange={(e) => setPassword2(e.target.value)}
      type="password"
      placeholder="Verify your password."
      required
      />
      </Form.Group>
      </Col>
      </Row>
      {isActive ? (
        <Button type="submit" className="reg-btn">
        Submit
        </Button>
        ) : (
        <Button className="reg-btn" onClick={e => notComplete(e)}>
        Submit
        </Button>
        )}
        </Form>
        </Col>
        </Row>
        <Col md={{ span: 5, offset: 3 }} className="text-center question">
        <p className="mt-5"> Already a member?<Container as={Link} to="/login" className="a-link"> Click here </Container>to Login</p>
        </Col>
        </Container>
      }
      </HelmetProvider>
      );
    }
