import { useEffect, useState, useContext } from 'react';
import { Container, Table } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom'
import AllProductsCard from '../components/AllProductsCard';
import UserContext from '../UserContext';
import { MdArrowBack } from 'react-icons/md'
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function AllProducts(){

  const {user} = useContext(UserContext)
  const [allProducts, setAllProducts] = useState();
  const [title, setTitle] = useState('')

  useEffect(() => {
    fetch(`https://hidden-stream-68962.herokuapp.com/products/all`)
    .then(res => res.json())
    .then(data => {
      setAllProducts(
        data.map(product => {
          return <AllProductsCard key={product._id} allProdProp={product} />
        })
        )
    })
    setTitle('All Products | Café Leblanc')
  }, [allProducts])

  let back = useNavigate()


  return (
    <HelmetProvider>
    <Helmet>
    <title>{ title ? title : "Café Leblanc" }</title>
    </Helmet>
    {
      !user.isAdmin ? 
      <Navigate to="/products" />
      : 
      <Container className="mt-3">
      <div onClick={() => back(-1)} className="back-btn">
      <MdArrowBack size={40} className=""></MdArrowBack>
      Back
      </div>
      <h2 className="text-center mb-4">All Products</h2>
      <Table responsive="sm" hover>
      <thead className="thead">
      <tr>
      <th>Product</th>
      <th>Name</th>
      <th className="text-center">Description</th>
      <th>Price</th>
      <th className="text-center">Actions</th>
      </tr>
      </thead>
      <tbody>{allProducts}</tbody>
      </Table>
      </Container>
    }
    </HelmetProvider>
    )
}