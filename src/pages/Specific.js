import { useState, useEffect, useContext} from 'react';
import { Container, Button, Modal, Col, Image, Form, InputGroup} from 'react-bootstrap';
import {useParams, useNavigate, Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import {FiPlus, FiMinus} from 'react-icons/fi'
import { MdArrowBack } from 'react-icons/md'
import Swal from 'sweetalert2';
import { Helmet, HelmetProvider } from 'react-helmet-async';

export default function Specific(){

  const {user} = useContext(UserContext);
  const [title, setTitle] = useState('')
  const [id, setId] = useState('')
  const {productId} = useParams()
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0)
  const [quantity, setQuantity] = useState(1)
  const [productStatus, setProductStatus] = useState(true)
  const [notFound, setNotFound] = useState(false)

  const [productName, setProductName] = useState('')
  const [productDescription, setProductDescription] = useState('')
  const [productPrice, setProductPrice] = useState('')
  const [showEdit, setShowEdit] = useState('')


  useEffect(() => {
    fetch(`https://hidden-stream-68962.herokuapp.com/products/${productId}`)
    .then(res=> res.json())
    .then(data => {
      if(data == false){
        setNotFound(true)
      } else {
        setProductStatus(data.isActive)
        setId(data._id)
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      }
    })
    
    setTitle(`${name} | Café Leblanc`)
  }, [productId, productStatus, name, description, productPrice, title])

  const add = (e) => {
    setQuantity(quantity + 1)
  }

  const minus = (e) => {
    setQuantity(quantity - 1)
  }

  let back = useNavigate()

  const addCart = (e) => {
    let existItem = false
    let productIndex
    let cart = []

    if(localStorage.getItem('cart')){
      cart = JSON.parse(localStorage.getItem('cart'))
    }

    for(let i=0; i< cart.length; i++){
      console.log(`Product id: ${cart[i]}`)
      if(cart[i].productId === id){
        existItem = true
        productIndex = i
      }
    }

    if(existItem){
      cart[productIndex].quantity += quantity
      cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
    } else{
      cart.push({
        'productId': id,
        'name': name,
        'price': price,
        'quantity': quantity,
        'subtotal': price * quantity
      })
    }

    localStorage.setItem('cart', JSON.stringify(cart))

    const Toast = Swal.mixin({
      toast: true,
      iconColor: 'black',
      position: 'bottom-end',
      showConfirmButton: false,
      timer: 3000,
      showCloseButton: true,
      customClass: {
        popup: 'add-cart'
      }
    })
    
    Toast.fire({
      icon: 'success',
      title: 'Added to Cart',
      footer: '<a href="/myCart" class="link-toast">View Cart</a>'
    })
  }

  const order = (e) => {
    let existItem = false
    let productIndex
    let cart = []

    if(localStorage.getItem('cart')){
      cart = JSON.parse(localStorage.getItem('cart'))
    }

    for(let i=0; i< cart.length; i++){
      console.log(`Product id: ${cart[i]}`)
      if(cart[i].productId === id){
        existItem = true
        productIndex = i
      }
    }

    if(existItem){
      cart[productIndex].quantity += quantity
      cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
    } else{
      cart.push({
        'productId': id,
        'name': name,
        'price': price,
        'quantity': quantity,
        'subtotal': price * quantity
      })
    }

    localStorage.setItem('cart', JSON.stringify(cart));
  }

  const archive = (e) => {
    fetch(`https://hidden-stream-68962.herokuapp.com/products/${productId}/archive`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(data => {
      setProductStatus(false)
    })
  }

  const unarchive = (e) => {
    fetch(`https://hidden-stream-68962.herokuapp.com/products/${productId}/unarchive`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(data => {
      setProductStatus(true)
    })
  }

//   // Edit Modal
//   const openEdit = (productId) => {
//     setProductName(name)
//     setProductDescription(description)
//     setProductPrice(price)
// }

// const closeEdit = () => {
//     setProductName('')
//     setProductDescription('')
//     setProductPrice(0)
// }

const src=`/images/products/${productId}.png`

    // ===========================================================
    // Opening Edit Modal
    const openEdit = (productId) => {
      setId(productId)
      setProductName(name)
      setProductDescription(description)
      setProductPrice(price)
      setProductStatus(productStatus)
      setShowEdit(true)
    }

  // Closing Edit Modal
  const closeEdit = () => {
    setProductName('')
    setProductDescription('')
    setProductPrice(0)
    setShowEdit(false)
  }

  // Edit Product
  const editProduct = (e, productId) => {
    e.preventDefault()

    Swal.fire({
      title: 'Do you want to save the changes?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      denyButtonText: 'No',
      customClass: {
        actions: 'my-actions',
        cancelButton: 'order-3',
        confirmButton: 'order-1 left-end',
        denyButton: 'order-2',
      }
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`https://hidden-stream-68962.herokuapp.com/products/${productId}`, {
          method: "PUT",
          headers: {
            'Content-type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
            name: productName,
            description: productDescription,
            price: productPrice
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          if(data){
            
            Swal.fire('Saved!', '', 'success')
            setProductName('')
            setProductDescription('')
            setProductPrice('')
            closeEdit()
          } else {
            alert('Something went wrong.')
            closeEdit()
          }
        })
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      } else {
        closeEdit()
      }
    })
  }
  
  
  return(
    <HelmetProvider>
    <Helmet>
    <title>{ title ? title : "Café Leblanc" }</title>
    </Helmet>
    {
      notFound ?
      <Navigate to="/404NotFound" />
      :
        // Container for the whole page
        <>
        <Container>
        <div onClick={() => back(-1)} className="back-btn mt-3">
        <MdArrowBack size={40} className=""></MdArrowBack>
        Back
        </div>
        <Container className="wrapper mt-5">
      {/* Container for the image */}
      <Container className="img-container">
      <Image className="product-img mt-5" src={src}></Image>
      </Container>
      
    {/* Container for info, price, etc */}
    <Container className="info-container mt-5">
  {/* Container for Title */}
  
  <h2 className="title">{name}</h2>

{/* Container for Description */}
<h6 className="description">{description}</h6>

{/* Container for Price */}
<h3 className="price">₱{price}.00</h3>

{/* Horizontal Line */}
<div className="line">{''}</div>

{/* Container for Quantity */}
<Container className="quantity-container">
{
  !user.isAdmin ?
  <>
  <h5 className="quantity-title mb-4">Quantity</h5>

  <InputGroup className="quantity-selector" size="sm">
  {
    quantity < 2 ?
    <Button className="quantity-btn minus" variant="outline-dark" onClick={e => minus(e)} disabled><FiMinus></FiMinus></Button>
    :
    <Button className="quantity-btn minus" variant="outline-dark" onClick={e => minus(e)}><FiMinus></FiMinus></Button>
  }
  <Col lg={2} xs={3}>
  <Form.Control
  className="text-center"
  value={quantity}
  onChange={(e) => setQuantity(Number(e.target.value))}
  type= "text"
  />
  </Col>
  
  <Button className="quantity-btn add" variant="outline-dark" onClick={e => add(e)}><FiPlus></FiPlus></Button>
  </InputGroup>
  </>
  :
  <Container className="d-none">
  <h5 className="quantity-title mb-4">Quantity</h5>

  <InputGroup className="quantity-selector" size="sm">
  {
    quantity < 2 ?
    <Button className="quantity-btn minus" variant="outline-dark" onClick={e => minus(e)} disabled><FiMinus></FiMinus></Button>
    :
    <Button className="quantity-btn minus" variant="outline-dark" onClick={e => minus(e)}><FiMinus></FiMinus></Button>
  }
  <Col lg={2} xs={3}>
  <Form.Control
  className="text-center"
  value={quantity}
  onChange={(e) => setQuantity(Number(e.target.value))}
  type= "text"
  />
  </Col>

  <Button className="quantity-btn add" variant="outline-dark" onClick={e => add(e)}><FiPlus></FiPlus></Button>
  </InputGroup>
  </Container>
}
{/* Add to Cart Button */}
{
  user.isAdmin ?
  <Button variant="outline-dark" className="cart-btn mt-5" onClick={(e)=> openEdit(e)}>Edit Product</Button>
  :
  <>
  <Button variant="outline-dark" className="cart-btn mt-5" onClick={ e => addCart(e) }>Add to Cart</Button>
  <Button variant="outline-dark" className="order-btn mt-5" onClick={ e => order(e) } as={Link} to="/myCart">Order Now</Button>
  </>
}

</Container>

</Container>
</Container>
</Container>

<Modal show={showEdit} onHide={closeEdit} centered>
<Form onSubmit={e => editProduct(e, id)}>
<Modal.Header closeButton>
<Modal.Title>Edit Product</Modal.Title>
</Modal.Header>
<Modal.Body>
<Form.Group controlId="productName">
<Form.Label>Name:</Form.Label>
<Form.Control type="text" placeholder="Enter product name" value={productName} onChange={e => setProductName(e.target.value)} required/>
</Form.Group>

<Form.Group controlId="productDescription">
<Form.Label>Description:</Form.Label>
<Form.Control type="text" placeholder="Enter product description" value={productDescription} onChange={e => setProductDescription(e.target.value)} required/>
</Form.Group>

<Form.Group controlId="productPrice">
<Form.Label>Price:</Form.Label>
<Form.Control type="number" placeholder="Enter product price" value={productPrice} onChange={e => setProductPrice(e.target.value)} required/>
</Form.Group>

<Form.Group controlId="productAvailability" className="mt-3">
<Form.Label>Available:</Form.Label>
{
  productStatus ? 
  <Button variant="outline-dark" className="actions-btn" onClick={ e => archive(e) }>In Stock</Button>
  :
  <Button variant="outline-dark" className="actions-btn" onClick={ e => unarchive(e) }>Out of Stock</Button>
}

</Form.Group>

</Modal.Body>
<Modal.Footer>
<Button variant="outline-dark" onClick={closeEdit}>Close</Button>
<Button variant="outline-dark" type="submit">Submit</Button>
</Modal.Footer>
</Form>	
</Modal>
</>
}
</HelmetProvider>
)

}