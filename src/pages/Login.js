import React, {useState, useEffect, useContext} from 'react';
import {Container, Form, Button, Row, Col} from 'react-bootstrap'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Link, Navigate, useNavigate} from 'react-router-dom'
import { Helmet, HelmetProvider } from 'react-helmet-async'

export default function Login(){
    const {user, setUser} = useContext(UserContext);

    let navigate = useNavigate()

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [title, setTitle] = useState('')
    
    function loginUser(e){
        e.preventDefault();

        fetch(`https://hidden-stream-68962.herokuapp.com/users/login`, {
            method: "POST",
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data.access !== 'undefined'){
                localStorage.setItem('token', data.access)
                userDetails(data.access)

                Swal.fire({
                    title: "Login Successful",
                    icon: "success"
                })

                navigate(-1)
            } else {
                Swal.fire({
                    title: "Login failed",
                    icon: "error",
                    text: "Incorrect email or password"
                })
                setPassword('')
            }

        })
    }

    const userDetails = (token) => {
        fetch(`https://hidden-stream-68962.herokuapp.com/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then (data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                name: data.firstName
            })
        })
    }

    useEffect(() => {
        if (email !== '' && password !== ''){
            setIsActive(true)
        } else {
            setIsActive(false)
        }

        setTitle("Login | Café Leblanc")
    }, [email, password, title])


    const notComplete = (e) => {
        const Toast = Swal.mixin({
          toast: true,
          iconColor: 'white',
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          showCloseButton: true,
          customClass: {
            popup: 'colored-toast'
        }
    })
        
        Toast.fire({
          icon: 'error',
          title: 'Please input your email and password'
      })
    }
    
    return (
      <HelmetProvider>
      <Helmet>
      <title>{ title ? title : "Café Leblanc"}</title>
      </Helmet>
      {
        (user.id !==null) ?
        <Navigate to="/" />
        :
        <Container className="mt-5">
        <Row>
        <Col md={{ span: 5, offset: 3 }} className="form-div">
        <Form className="mb-3" onSubmit={(e) => loginUser(e)}>
        <h1 className="register mb-4">Login</h1>
        <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        type="email"
        placeholder="Enter email"
        required
        />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        type="password"
        placeholder="Password"
        required
        />
        </Form.Group>
        
        {isActive ? (
          <Button type="submit" className="reg-btn">
          Submit
          </Button>
          ) : (
          <Button className="reg-btn" onClick={e => notComplete(e)}>
          Submit
          </Button>
          )}
          </Form>
          </Col>
          </Row>
          
          <Col md={{ span: 5, offset: 3 }} className="text-center question">
          <p className="mt-5"> Not a member?<Container as={Link} to="/register" className="a-link"> Click here </Container>to Register</p>
          </Col>
          </Container>
      }
      </HelmetProvider>
      )
  }
  