import { useState, useEffect } from 'react'
import { Container, Stack, CardGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import ProductCard from './ProductCard';

export default function Drinks(){

  const [products, setProducts] = useState([])
  let foods = /Food/

  useEffect(() => {
    fetch(`https://hidden-stream-68962.herokuapp.com/products`)
    .then(res => res.json())
    .then(data => {
      let filtered = data.filter(x => {
        return foods.test(x.category)
      })
      // console.log(filtered)
      setProducts(
        filtered.slice(0,4).map((product) => {
          return <ProductCard key={product._id} prodProp={product} />
        })
        )
    })
  }, [])

  

  return (
   <Container className="mt-4 mb-5">
   <Stack direction="horizontal" className="mb-3">
   <h2>Food Menu</h2>
   <Link to="/food" className="ms-auto see-all">
   See All
   </Link>
   </Stack>
   <CardGroup>{products}</CardGroup>
   </Container>
   )
  
}