import { useState, useEffect } from 'react'
import { Container, Stack, CardGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import ProductCard from './ProductCard';

export default function Drinks(){

  const [products, setProducts] = useState([])
  let drinks = /Drinks/

  useEffect(() => {
    fetch(`https://hidden-stream-68962.herokuapp.com/products`)
    .then(res => res.json())
    .then(data => {
      let filtered = data.filter(x => {
        return drinks.test(x.category)
      })
      // console.log(filtered)
      setProducts(
        filtered.slice(0,4).map((product) => {
          return <ProductCard key={product._id} prodProp={product} />
        })
        )
    })
  }, [])

  

  return (
   <Container className="mb-3">
   <Stack direction="horizontal" className="mb-3">
   <h2>Drinks Menu</h2>
   <Link to="/drinks" className="ms-auto see-all">
   See All
   </Link>
   </Stack>
   <CardGroup>{products}</CardGroup>
   </Container>
   )
  
}