import React from 'react';
import { Carousel } from 'react-bootstrap';

export default function Featured(){
  return (
    <Carousel fade indicators={false} className="mb-4">
    <Carousel.Item className="carousel-item" interval={1500}>
    <img
    className="d-block w-100"
    src="/images/curry.png"
    alt="First slide"
    />
    </Carousel.Item>
    <Carousel.Item className="carousel-item" interval={1500}>
    <img
    className="d-block w-100"
    src="/images/kani-salad.png"
    alt="Second slide"
    />
    </Carousel.Item>
    <Carousel.Item className="carousel-item" interval={1500}>
    <img
    className="d-block w-100"
    src="/images/latte2.png"
    alt="Third slide"
    />
    </Carousel.Item>
    </Carousel>
    );
}