import React from 'react';

export default function Footer(){

    const date = new Date().getFullYear()

    return(
        <div className="footer mt-auto">
        <h6 className="footer-text mt-2">Shibuya, Tokyo</h6>
        <h6 className="footer-text mb-0">Cafe Leblanc | {date}</h6>
        </div>
        )
    }