import { Button, Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext, useState } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductCard({prodProp}){

    const {user} = useContext(UserContext)
    const { name, price, _id } = prodProp
    const [id, setId] = useState(_id)
    const [quantity, setQuantity] = useState(1)

    const addCart = (e) => {
      let existItem = false
      let productIndex
      let cart = []

      if(localStorage.getItem('cart')){
        cart = JSON.parse(localStorage.getItem('cart'))
      }

      for(let i=0; i< cart.length; i++){
        console.log(`Product id: ${cart[i]}`)
        if(cart[i].productId === id){
          existItem = true
          productIndex = i
        }
      }

      if(existItem){
        cart[productIndex].quantity += quantity
        cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
      } else{
        cart.push({
          'productId': id,
          'name': name,
          'price': price,
          'quantity': quantity,
          'subtotal': price * quantity
        })
      }

      localStorage.setItem('cart', JSON.stringify(cart))

       const Toast = Swal.mixin({
          toast: true,
          iconColor: 'black',
          position: 'bottom-end',
          showConfirmButton: false,
          timer: 3000,
          showCloseButton: true,
          customClass: {
            popup: 'add-cart'
          }
        })
        
        Toast.fire({
          icon: 'success',
          title: 'Added to Cart',
          footer: '<a href="/myCart" class="link-toast">View Cart</a>'
        })
    }

    const src=`/images/products/${_id}.png`

    return (
      <Col lg={3} md={4}>
        <Card className="text-center drinks-highlight">
          <Link to={`/product/${_id}`}>
            <Card.Img variant="top" src={src} className="prod-img" />
          </Link>
          <Card.Body className="d-flex flex-column">
            <Link to={`/product/${_id}`} className="prod-link">
              <Row className="mt-1 mb-3">
                <Col>
                  <Card.Title>{name}</Card.Title>
                </Col>
                <Col>
                  <Card.Title>₱{price.toFixed(2)}</Card.Title>
                </Col>
              </Row>
            </Link>
            {
              user.isAdmin ? 
              <Button 
                variant="primary" 
                className="drinks-btn mt-auto"
                as = {Link}
                to = {`/product/${_id}`}
              >
                Edit Product
              </Button>
             : 
              <Button
                variant="primary"
                className="drinks-btn mt-auto"
                onClick={(e) => addCart(e)}
              >
                Add to Cart
              </Button>
            
            }
          </Card.Body>
        </Card>
      </Col>
    );
}